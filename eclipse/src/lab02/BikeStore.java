//Vashti Lanz Rubio
//1931765
package lab02;

public class BikeStore {
	public static void main(String[] args) {
		Bicycle[] bikes = new Bicycle[4];
		bikes[0] = new Bicycle("Good",4,70);
		bikes[1] = new Bicycle("Bad",1,30);
		bikes[2] = new Bicycle("Best",6,100);
		bikes[3] = new Bicycle("Knockoff",10,200);
		
		for(Bicycle b : bikes) {
			System.out.println(b);
		}
	}
}
